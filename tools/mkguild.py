#!/usr/bin/env python3

import os
import discord

client = discord.Client()

@client.event
async def on_ready():
    print('Logged on as {0}!'.format(client.user))
    guild = await client.create_guild(name = "overlord testing grounds")
    channel = await guild.create_text_channel("general")
    print(await channel.create_invite())

client.run(os.environ["TOKEN"])
