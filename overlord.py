#!/usr/bin/env python3

import os
import discord

client = discord.Client()

@client.event
async def on_ready():
    print('Logged on as {0}!'.format(client.user))

@client.event
async def on_message(message):
    print(message.content)

client.run(os.environ["TOKEN"])
